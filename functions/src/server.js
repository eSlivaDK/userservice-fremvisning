const express = require("express");
const cors = require("cors");
const app = express();
const allowedOrigins = ["http://localhost:4200", "http://localhost:8080"];
const PORT = process.env.PORT || 8081;
const HOST = "0.0.0.0";
const mqttSetup = require("./lib/MQService/setupMqtt");
app.use(cors({ origin: true }));
app
  .use(express.urlencoded({ extended: true }))
  .use(express.json())
  .use("/users", require("./lib/users/route"))
  .use("/notifications", require("./lib/notifications/route"))
  .get("*", (_, res) =>
    res.status(404).json({ success: false, data: "Endpoint not found" })
  );
app.listen(PORT, HOST);
mqttSetup.setup();

console.log(`Running on http://${HOST}:${PORT}`);
module.exports = app;
