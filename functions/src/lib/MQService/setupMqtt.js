require("dotenv").config();
const amqp = require("amqplib");
// RabbitMQ connection string
const messageQueueConnectionString = process.env.CLOUDAMQP_URL;
async function setup() {
  console.log("Setting up RabbitMQ Exchanges/Queues");
  // connect to RabbitMQ Instance
  try {
    let connection = await amqp.connect(messageQueueConnectionString);

    // create a channel
    let channel = await connection.createChannel();

    // create exchange
    await channel.assertExchange("user.tx", "topic", { durable: true });
    await channel.assertExchange("competition.tx", "topic", {
      durable: true
    });

    // create user queues

    await channel.assertQueue("user-created-queue", { durable: true });
    await channel.assertQueue("user-updated-queue", { durable: true });
    await channel.assertQueue("user-deleted-queue", { durable: true });
    await channel.assertQueue("user-requested.badge-queue", {
      durable: true
    });

    /*
    // create competition queues
    await channel.assertQueue("competition-created-queue", { durable: true });
    await channel.assertQueue("competition-updated-queue", { durable: true });
    await channel.assertQueue("competition-deleted-queue", { durable: true });
    await channel.assertQueue("competition-requested-queue", { durable: true });
*/
    // bind queues
    await channel.bindQueue("user-created-queue", "user.tx", "created");
    await channel.bindQueue("user-updated-queue", "user.tx", "updated");
    await channel.bindQueue("user-deleted-queue", "user.tx", "deleted");
    //await channel.bindQueue("user-requested.badge-queue", "user.tx", "topic");
    console.log("3");
    console.log("Setup DONE");
  } catch (error) {
    console.log("TCL: setup -> error", error);
  }
}

module.exports = {
  setup
};
