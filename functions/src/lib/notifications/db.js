const admin = require("firebase-admin");
const db = admin.firestore();
const userCollectionName = "users";
const notificationCollectionName = "notifications";

async function getPaginatedNotifications(userId, index) {
  let notifications = [];
  const userRef = db.collection(userCollectionName).doc(userId);
  const notificationRef = userRef
    .collection(notificationCollectionName)
    .orderBy("createdAt")
    .limit(index);

  let first = notificationRef;
  let paginate = await first.get();

  // Get the last document
  let last = paginate.docs[paginate.docs.length - 1];

  // Construct a new query starting at this document.
  // Note: this will not have the desired effect if multiple
  // cities have the exact same population value.
  let next = await userRef
    .collection("notifications")
    .orderBy("createdAt")
    .startAt(last.data().createdAt)
    .limit(10)
    .get();

  next.forEach(doc => {
    notifications.push(doc.data());
  });

  return {
    data: notifications
  };
}

async function addNotification(userId, notification) {
  const userRef = db.collection(userCollectionName).doc(userId);
  const notificationRef = userRef.collection(notificationCollectionName);
  await notificationRef.add(notification);
  return {
    data: "user: " + userId + " received a new notification"
  };
}
async function updateNotificationStatus(userId, notificationId) {
  const userRef = db.collection(userCollectionName).doc(userId);
  const notificationRef = userRef
    .collection(notificationCollectionName)
    .doc(notificationId);
  await notificationRef.update({
    read: true
  });
  return {
    data: "notification: " + notificationId + " has been read"
  };
}
async function deleteNotification(userId, notificationId) {
  const userRef = db.collection(userCollectionName).doc(userId);
  const notificationRef = userRef
    .collection(notificationCollectionName)
    .doc(notificationId);
  await notificationRef.delete();
  return {
    data: "notification: " + notificationId + " has been deleted"
  };
}
module.exports = {
  getPaginatedNotifications,
  addNotification,
  updateNotificationStatus,
  deleteNotification
};
