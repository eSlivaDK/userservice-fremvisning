const db = require("./db");

async function updateUserDoc(req, res) {
  const body = req.body;
  try {
    return res.json({
      success: true,
      data: await db.updateUserDoc(body)
    });
  } catch (error) {
    return res.json({
      success: false,
      data: error
    });
  }
}

async function getUserDoc(req, res) {
  const userId = req.params.userId;
  try {
    return res.json({
      success: true,
      data: await db.getUserDoc(userId)
    });
  } catch (error) {
    return res.json({
      success: false,
      data: error
    });
  }
}

async function createUserDoc(req, res) {
  const body = req.body;
  try {
    return res.json({
      success: true,
      data: await db.createUserDoc(body)
    });
  } catch (error) {
    console.log("TCL: createUserDoc -> error", error);
    return res.json({
      success: false,
      data: error.toString()
    });
  }
}

async function addCompetitionToUser(req, res) {
  const body = req.body;
  const competitionId = body.competitionId;
  const userId = body.userId;

  try {
    return res.json({
      success: true,
      data: await db.addCompetitionToUser(userId, competitionId)
    });
  } catch (error) {
    return res.json({
      success: false,
      data: error.toString()
    });
  }
}
async function addBadgeToUser(req, res) {
  const body = req.body;
  const badgeId = body.badgeId;
  const userId = body.userId;
  try {
    return res.json({
      success: true,
      data: await db.addBadgeToUser(userId, badgeId)
    });
  } catch (error) {
    return res.json({
      success: false,
      data: error
    });
  }
}
async function addIdeaToUser(req, res) {
  const body = req.body;
  const userId = body.userId;
  const ideaId = body.ideaId;
  try {
    return res.json({
      success: true,
      data: await db.addIdeaToUser(userId, ideaId)
    });
  } catch (error) {
    return res.json({
      success: false,
      data: error
    });
  }
}

module.exports = {
  addCompetitionToUser,
  addBadgeToUser,
  addIdeaToUser,
  createUserDoc,
  getUserDoc,
  updateUserDoc
};
