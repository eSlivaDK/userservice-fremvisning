require("dotenv").config();

const admin = require("firebase-admin");
const db = admin.firestore();
const userCollectionName = "users";
const amqp = require("amqplib");
const messageQueueConnectionString = process.env.CLOUDAMQP_URL;

async function updateUserDoc(user) {
  const userId = user.uid;
  let userRef = db.collection(userCollectionName).doc(userId);
  await userRef.update(user);
  return user;
}

async function getUserDoc(userId) {
  const userRef = db.collection(userCollectionName).doc(userId);
  const doc = await userRef.get();
  if (doc.exists) {
    const data = doc.data();
    return data;
  }
  return undefined;
}

async function createUserDoc(user) {
  const userId = user.uid;
  const userRef = db.collection(userCollectionName).doc(userId);
  await userRef.set(user);
  publishToChannel("created", "user.tx", user);
  return user;
}

async function addCompetitionToUser(userId, competitionId) {
  const userRef = db.collection(userCollectionName).doc(userId);

  await userRef.update({
    myCompetitions: admin.firestore.FieldValue.arrayUnion(competitionId)
  });
  const data = competitionId + " was added to user: " + userId;
  return data;
}
async function addBadgeToUser(userId, badgeId) {
  const userRef = db.collection(userCollectionName).doc(userId);

  await userRef.update({
    myBadges: admin.firestore.FieldValue.arrayUnion(badgeId)
  });
  const data = badgeId + " was added to user: " + userId;

  return data;
}
async function addIdeaToUser(userId, ideaId) {
  const userRef = db.collection(userCollectionName).doc(userId);
  await userRef.update({
    myIdeas: admin.firestore.FieldValue.arrayUnion(ideaId)
  });
  const data = ideaId + " was added to user: " + userId;

  return data;
}
async function publishToChannel(routingKey, exchangeName, data) {
  // connect to Rabbit MQ and create a channel
  let connection = await amqp.connect(messageQueueConnectionString);
  let channel = await connection.createConfirmChannel();
  try {
    channel.publish(
      exchangeName,
      routingKey,
      Buffer.from(JSON.stringify(data), "utf-8")
    );
    // This line doesn't run until the server responds to the publish
    await channel.close();
    // This line doesn't run until the client has disconnected without error
    console.log("Done");
  } catch (e) {
    // Do something about it!
    console.log(e.stack);
  }
}

module.exports = {
  addCompetitionToUser,
  addBadgeToUser,
  addIdeaToUser,
  createUserDoc,
  getUserDoc,
  updateUserDoc
};
