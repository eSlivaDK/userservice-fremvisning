const functions = require("firebase-functions");
const admin = require("firebase-admin");
const serviceAccount = require("../dev-user-service-account-file.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://dev-user-service-352a1.firebaseio.com"
});
const server = require("./src/server");

const api = functions
  .runWith({
    memory: "2GB",
    timeoutSeconds: 120
  })
  .https.onRequest(server);
module.exports = {
  api
};
