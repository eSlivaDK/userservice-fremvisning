FROM node:8-alpine
WORKDIR /userService/functions
COPY . /userService
RUN npm install -g firebase-tools
RUN npm install
ENV GOOGLE_APPLICATION_CREDENTIALS /userService/dev-user-service-account-file.json
EXPOSE 8081

CMD [ "node", "index.js" ]
